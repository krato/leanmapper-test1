<?php

namespace App\Presenters;

use Model\Repositories\UserRepository;
use Nette;

class HomepagePresenter extends Nette\Application\UI\Presenter
{

	/** @var UserRepository @inject */
	public $userRepository;

	public function renderDefault()
	{
		$this->template->users = $this->userRepository->findAll();
	}

}
