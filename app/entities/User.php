<?php

namespace Model\Entity;

/**
 * Description of User
 * 
 * @property-read int $id
 * @property string $role m:enum(self::ROLE_*)
 * @property string $login
 * @property string $password
 * @property Article[] $articles m:belongsToMany(author_id)
 *
 * @author krato
 */
class User extends \LeanMapper\Entity
{

	const ROLE_USER = 'user';
	const ROLE_REDAKTOR = 'redaktor';
	const ROLE_ADMIN = 'admin';
	const ROLE_SUPERADMIN = 'superadmin';

}
