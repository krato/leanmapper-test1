<?php

namespace Model\Entity;

/**
 * Description of User
 *
 * @property-read int $id
 * @property \DibiDateTime $created
 * @property User|null $author m:hasOne(author_id)
 * @property string $title
 * @property string $body
 *
 * @author krato
 */
class Article extends \LeanMapper\Entity
{

}
