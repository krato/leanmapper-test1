<?php

namespace Model\Repositories;

/**
 * Description of BaseRepository
 *
 * @author krato
 */
abstract class BaseRepository extends \LeanMapper\Repository
{

	public function find($id)
	{
		$row = $this->connection->select('*')
				->from($this->getTable())
				->where('id = %i', $id)
				->fetch();

		if ($row === false)
		{
			throw new \Exception('Entity was not found.');
		}
		return $this->createEntity($row);
	}

	public function findAll()
	{
		return $this->createEntities(
						$this->connection->select('*')
								->from($this->getTable())
								->fetchAll()
		);
	}

}
