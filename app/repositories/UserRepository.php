<?php

namespace Model\Repositories;

/**
 * Description of UserRepository
 * 
 * @method \Model\Entity\User find(int $id)
 * @method \Model\Entity\User[] findAll()
 *
 * @author krato
 */
class UserRepository extends BaseRepository
{

}
